route users :
    -.(post)/users/register : daftar user baru,
    dibutuhkan request body = username,password,role(SA atau player)

    -.(post)/users/login : login user yg sudah terdaftar
    dibutuhkan request body = username,password

    -.(get)/users/history : mencari history dari permainan yg sudah dilakukan
    dibutuhkan request token dari header = auth

route rooms :
    -.(post)/rooms/create : membuat room baru
    dibutuhkan request body = name
    dan dibutuhkan request token dr header = auth

    -.(post)/rooms/fight/:idroom : input pilihan permainan
    dibutuhkan request body = playerChoice(rock/paper/scissors)
    dan juga request token dari header = auth
(maximal 2 user per room)

    -.(post)/rooms/join/:idroom : input pemain yang mmasuk dalam room 
    dibutuhkan requet token dari header = auth
(maximal 2 user per room)

route admin :   
    -.(get)/admin/users : untuk menampilkan list user dan hanya bisa di akses oleh SA
    request header berisi token jwt = auth

    -.(get)/admin/rooms : untuk menampilkan list room dan hanya bisa di akses oleh SA
    request header berisi token jwt = auth

    -.(get)/admin/history : untuk menampilkan list history dari semua user dan hanya bisa di akses oleh SA
    request header berisi token jwt = auth